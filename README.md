# [Git Reference Guide][1]

### Topic Covered

1. Git vocabulary
2. Staging and commit
3. Restoring (checkout)
4. Push and pull
5. Some basics of terminal
6. Tracking changes in a folder (init)
7. Cloning
8. Pushing local repo to a repo hosting service.


### Most used commands at a glance
```
git init
git status
git add -A
git commit -m ”Your message here.”
git checkout – .
git clone REPO-URL
git remote add origin REPO-URL
git remote set-url origin –add REPO-URL
```
[1]: https://github.com/Az-21/learning-git/blob/master/reference/ref.pdf
